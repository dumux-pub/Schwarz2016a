## SUMMARY

This is the DuMuX module containing the code for producing the results
published in:


Schwarz, Florian

Effizienz von Druckluftspeicherung in porösen geologischen Formationen

Bachelor-Thesis, 2016

## Installation

The easiest way to install this module is to create a new folder and to execute the file

[installSchwarz2016a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Schwarz2016a/raw/master/installSchwarz2016a.sh)

in this folder.
For a detailed information on installation have a look at the DuMuX installation guide or use the DuMuX handbook, chapter 2.

## USED VERSIONS
The following version of DuMuX and Dune have been used:

- dumux                     release-2.9
- Schwarz2016a
- dune-common               releases/2.4
- dune-geometry             releases/2.4
- dune-grid                 releases/2.4
- dune-istl                 releases/2.4
- dune-localfunctions       releases/2.4
- dune-alugrid              releases/2.4

The content of this DUNE module was extracted from the module dumux. In particular, the following subfolders of dumux have been extracted:


- test/porousmediumflow/2p2c/implicit/CAES-Aquifer/,


Additionally, all headers in dumux that are required to build the executables from the sources


- test/porousmediumflow/2p2c/implicit/CAES-Aquifer/test_cc2p2cni.cc,


have been extracted. You can build the module just like any other DUNE module. For building and running the executables, please go to the folders containing 
the sources listed above.

## Installation with Docker

Create a new folder and change into it

```bash
mkdir Schwarz2016a
cd Schwarz2016a
```

Then download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/Schwarz2016a/-/raw/master/docker_schwarz2016a.sh
```

and open the docker container by running
```bash
bash docker_schwarz2016a.sh open
```

After the script has run successfully, executable can be built and run with an input file,
```bash
cd Schwarz2016a/build-cmake/test/porousmediumflow/2p2c/implicit/CAES-Aquifer/
make test_cc2p2cni
./test_cc2p2cni test_cc2p2cni.input
```
