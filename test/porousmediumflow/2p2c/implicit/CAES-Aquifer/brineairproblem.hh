
// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Non-isothermal gas injection problem where a gas (e.g. air)
 *        is injected into a fully water saturated medium.
 */
#ifndef DUMUX_BRINE_AIR_PROBLEM_HH
#define DUMUX_BRINE_AIR_PROBLEM_HH

// The DUNE grid used
#if  HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#elif HAVE_UG
#include <dune/grid/uggrid.hh>
#else
#include <dune/grid/yaspgrid.hh>
#endif // HAVE_DUNE_ALUGRID, HAVE_UG

#include <dumux/io/gnuplotinterface.hh>

//#include <dumux/material/fluidsystems/h2on2fluidsystem.hh>
#include <dumux/material/fluidsystems/h2oair.hh>
//#include <dumux/material/fluidsystems/brineairfluidsystem.hh>
#include <dumux/material/components/simpleh2o.hh>

#include <dumux/porousmediumflow/2p2c/implicit/model.hh>
// #include <dumux/implicit/common/implicitporousmediaproblem.hh>

//#include "waterairspatialparams.hh"
#include "brineairspatialparams.hh"

#define ISOTHERMAL 0

namespace Dumux
{
    bool isExtracting;
    bool reachedLowerBound;
template <class TypeTag>
class BrineAirProblem;

namespace Properties
{
NEW_TYPE_TAG(BrineAirProblem, INHERITS_FROM(TwoPTwoCNI, BrineAirSpatialParams));
NEW_TYPE_TAG(BrineAirBoxProblem, INHERITS_FROM(BoxModel, BrineAirProblem));
NEW_TYPE_TAG(BrineAirCCProblem, INHERITS_FROM(CCModel, BrineAirProblem));

// Set the grid type
// Set grid and the grid creator to be used
#if HAVE_DUNE_ALUGRID /*@\label{tutorial-coupled:set-grid}@*/
SET_TYPE_PROP(BrineAirProblem, Grid, Dune::ALUGrid</*dim=*/2, 2, Dune::cube, Dune::nonconforming>); /*@\label{tutorial-coupled:set-grid-ALU}@*/
#elif HAVE_UG
SET_TYPE_PROP(BrineAirProblem, Grid, Dune::UGGrid<2>);
#else
SET_TYPE_PROP(BrineAirProblem, Grid, Dune::YaspGrid<2>);
#endif // HAVE_DUNE_ALUGRID

// Set the problem property
SET_TYPE_PROP(BrineAirProblem, Problem, Dumux::BrineAirProblem<TypeTag>);





// Set the wetting phase
// SET_PROP(BrineAirProblem, FluidSystem)
// {
//   typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
//   typedef Dumux::FluidSystems::BrineAir<Scalar, Dumux::SimpleH2O<Scalar>> type;
// };

// Set the fluid system to use complex relations (last argument)
SET_TYPE_PROP(BrineAirProblem, FluidSystem,
              FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar),
                                   Dumux::H2O<typename GET_PROP_TYPE(TypeTag, Scalar)>, false>);

// Define whether mole(true) or mass (false) fractions are used
SET_BOOL_PROP(BrineAirProblem, UseMoles, false);

}


/*!
 * \ingroup TwoPTwoCModel
 * \ingroup ImplicitTestProblems
 * \brief Non-isothermal gas injection problem where a gas (e.g. air)
 *        is injected into a fully water saturated medium. During
 *        buoyancy driven upward migration the gas passes a high
 *        temperature area.
 *
 * The domain is sized 40 m times 40 m in a depth of 1000 m. The rectangular area
 * with the increased temperature (380 K) starts at (20 m, 1 m) and ends at
 * (30 m, 30 m).
 *
 * For the mass conservation equation neumann boundary conditions are used on
 * the top and on the bottom of the domain, while dirichlet conditions
 * apply on the left and the right boundary.
 * For the energy conservation equation dirichlet boundary conditions are applied
 * on all boundaries.
 *
 * Gas is injected at the bottom boundary from 15 m to 25 m at a rate of
 * 0.001 kg/(s m), the remaining neumann boundaries are no-flow
 * boundaries.
 *
 * At the dirichlet boundaries a hydrostatic pressure, a gas saturation of zero and
 * a geothermal temperature gradient of 0.03 K/m are applied.
 *
 * The model is able to use either mole or mass fractions. The property useMoles can be set to either true or false in the
 * problem file. Make sure that the according units are used in the problem setup. The default setting for useMoles is true.
 *
 * This problem uses the \ref TwoPTwoCModel and \ref NIModel model.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box2p2cni</tt> or
 * <tt>./test_cc2p2cni</tt>
 *  */
template <class TypeTag >
class BrineAirProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GridView::Grid Grid;

    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;
    
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef Dumux::H2O<Scalar> IapwsH2O;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

        wCompIdx = FluidSystem::wCompIdx,
        nCompIdx = FluidSystem::nCompIdx,
	
	nPhaseIdx = Indices::nPhaseIdx,
	wPhaseIdx = Indices::wPhaseIdx,
        energyEqIdx = Indices::energyEqIdx,

	
#if !ISOTHERMAL
        temperatureIdx = Indices::temperatureIdx,
    //  energyEqIdx = Indices::energyEqIdx,
#endif

        // Phase State
        wPhaseOnly = Indices::wPhaseOnly,
	bothPhases = Indices::bothPhases,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,

        conti0EqIdx = Indices::conti0EqIdx,
        contiNEqIdx = conti0EqIdx + Indices::nCompIdx,
        contiWEqIdx = conti0EqIdx + Indices::wCompIdx
     
    };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };

    typedef Dumux::Air<Scalar> Air;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    
    //! property that defines whether mole or mass fractions are used
    static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);

public:
    /*!
     * \brief The constructor.
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     */
    BrineAirProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        maxDepth_ = 500.0; // [m]
        eps_ = 1e-6;
	massInjected_ = 0.0;
	injectionPressure_ = 0.0;
	massInjectedTimestep_ = 0.0;

        FluidSystem::init();

        name_               = GET_RUNTIME_PARAM(TypeTag, std::string, Problem.Name);

        //stating in the console whether mole or mass fractions are used
        if(useMoles)
        {
            std::cout<<"problem uses mole-fractions"<<std::endl;
        }
        else
        {
            std::cout<<"problem uses mass-fractions"<<std::endl;
        }

        this->spatialParams().plotMaterialLaw();
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    { return name_; }

#if ISOTHERMAL
    /*!
     * \brief Returns the temperature within the domain.
     *
     * \param element The element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param scvIdx The local vertex index (SCV index)
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    {
        return 273.15 + 10; // -> 10°C
    }
#endif

    /*!
     * \brief Returns the source term at specific position in the domain.
     *
     * \param values The source values for the primary variables
     * \param globalPos The position
     *
     * The units must be according to either using mole or mass fractions. (mole/(m^3*s) or kg/(m^3*s))
     */
    void sourceAtPos(PrimaryVariables &values,
                     const GlobalPosition &globalPos) const
    {
        values = 0;
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position for which the bc type should be evaluated
     */
    void boundaryTypesAtPos(BoundaryTypes &values,
                            const GlobalPosition &globalPos) const
    
    {
    
    Scalar right = this->bBoxMax()[0];
    Scalar left = this->bBoxMin()[0];
    Scalar top = this->bBoxMax()[1];

        if(globalPos[0] > right - eps_ || globalPos[0] < eps_)     //Dirichlet rechts und links für den Druck
            values.setAllDirichlet();	    
        else
            values.setAllNeumann();                               

        if(globalPos[0] > right - eps_ || globalPos[0] < eps_)     //Outflow rechts und links für Energie      
            values.setOutflow(energyEqIdx);
        else
	{
// 	  if(globalPos[1] > top - eps_ && globalPos[0] > right / 2.0 -10.0 && globalPos[0] < right / 2.0 + 10.0)    //Bei dem Punkt für die Injektion Neumann
	  if(globalPos[1] > top - 1.0 && globalPos[0] > right / 2.0 -0.5 && globalPos[0] < right / 2.0)   
	  {
	    values.setNeumann(contiNEqIdx);
// 	    values.setNeumann(contiWEqIdx);
	  }
	   else
	     values.setDirichlet(energyEqIdx);                             // An den anderen Stellen oben und unten Dirichlet für die Energie
	}
	     
//         if(globalPos[0] > right - eps_ && globalPos[0] < left - eps_)
//             values.setAllDirichlet();
//         else
//             values.setAllNeumann();


    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The position for which the bc type should be evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        initial_(values, globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values The neumann values for the conservation equations
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param intersection The intersection between element and boundary
     * \param scvIdx The local vertex index
     * \param boundaryFaceIdx The index of the boundary face
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     *
     * The units must be according to either using mole or mass fractions. (mole/(m^2*s) or kg/(m^2*s))
     */
      void solDependentNeumann(PrimaryVariables &neumannValues,
                      const Element &element,
                      const FVElementGeometry &fvGeometry,
                      const Intersection &intersection,
                      const int scvIdx,
                      const int boundaryFaceIdx,
                      const ElementVolumeVariables &elemVolVars) const
    {
      neumannValues = 0;
      
      GlobalPosition globalPos;
      if (isBox)
	globalPos = element.geometry().corner(scvIdx);
      else
	globalPos = intersection.geometry().center();
      
      // negative values for injection
      Scalar time = this->timeManager().time()+this->timeManager().timeStepSize();
   
      Scalar top = this->bBoxMax()[1];
      Scalar right = this->bBoxMax()[0];
      Scalar left = this->bBoxMin()[0];
      
      FluidState fs = elemVolVars[scvIdx].fluidState();
      
      Scalar cellHeight = 0.41;// für gebogenes Gitter
      Scalar effectiveWellRadius = 0.2*cellHeight;
      Scalar WellDiameter = 0.5335;
      Scalar permeability = this->spatialParams().intrinsicPermeability(element,fvGeometry,scvIdx);
    
      Scalar mobilityGas = std::max(0.1, elemVolVars[scvIdx].relativePermeability(nPhaseIdx)/fs.viscosity(nPhaseIdx));
      Scalar mobilityWater = std::max(0.1, elemVolVars[scvIdx].relativePermeability(wPhaseIdx)/fs.viscosity(wPhaseIdx));
      Scalar WellIndex =(2.0*M_PI*cellHeight)/log(effectiveWellRadius/WellDiameter);
      Scalar InjectionPressure;

      
//       Scalar ProductionPressure = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, ProductionPressure);
      Scalar ProductionPressure;
      Scalar densityNw = std::max(fs.density(nPhaseIdx), 60.0);
      Scalar densityW = std::max(fs.density(wPhaseIdx), 800.0);
      
      
//     if(globalPos[dim-1] > this->bBoxMax()[dim-1] - 0.01)
      if(globalPos[1] > top - 1.0 && globalPos[0] > right / 2.0 -0.5 && globalPos[0] < right / 2.0)//für dome-Gitter
//       if(globalPos[1] > top - eps_ && globalPos[0] > right / 2.0 -10.0 && globalPos[0] < right / 2.0 + 10.0)//für Rechteck-Gitter
      {
	
       if(time > 1.0)
	 {
	  
	   if(massInjected_ >= 2000.0)
	   {
	     isExtracting = true;
	   }
	
	   else if(massInjected_ < 2000.0 && reachedLowerBound)
	   {
	     isExtracting = false;
	     reachedLowerBound = false;
	   }
	
	   if(!isExtracting)
	   {
	     injectionPressure_ = 80.0e5;
	     
	     neumannValues[contiNEqIdx] = mobilityGas*WellIndex*densityNw*permeability*(injectionPressure_-fs.pressure(nPhaseIdx))
	     *(1.0/intersection.geometry().volume()); //[kg/m²s]
	   }
	   else
	   {
	     if(massInjected_ <= 0.0)
	     {
	       reachedLowerBound = true;
	     }
	     else
	     {
	       neumannValues[contiNEqIdx] = 0.150 / intersection.geometry().volume();
	       
// 	       neumannValues[contiNEqIdx]=(fs.massFraction(nPhaseIdx,Indices::nCompIdx)*mobilityGas*WellIndex*densityNw*permeability
//                     *(ProductionPressure-fs.pressure(nPhaseIdx)) + fs.massFraction(wPhaseIdx,Indices::nCompIdx)*mobilityWater*
//                             WellIndex*densityW*permeability*(ProductionPressure-fs.pressure(wPhaseIdx)))*(1.0/intersection.geometry().volume());
	  
// 	       neumannValues[contiWEqIdx]=(fs.massFraction(nPhaseIdx,Indices::wCompIdx)*mobilityGas*WellIndex*densityNw*permeability
//                     *(ProductionPressure-fs.pressure(nPhaseIdx)) + fs.massFraction(wPhaseIdx,Indices::wCompIdx)*mobilityWater*
//                             WellIndex*densityW*permeability*(ProductionPressure-fs.pressure(wPhaseIdx)))*(1.0/intersection.geometry().volume());

	     	injectionPressure_=(neumannValues[contiNEqIdx]*(intersection.geometry().volume())+fs.massFraction(nPhaseIdx,Indices::nCompIdx)*mobilityGas*WellIndex*densityNw*permeability
                     *fs.pressure(nPhaseIdx)+fs.massFraction(wPhaseIdx,Indices::nCompIdx)*mobilityWater*
                             WellIndex*densityW*permeability*fs.pressure(wPhaseIdx))*(1.0/(fs.massFraction(nPhaseIdx,Indices::nCompIdx)*mobilityGas*WellIndex*densityNw*permeability
				  +fs.massFraction(wPhaseIdx,Indices::nCompIdx)*mobilityWater*WellIndex*densityW*permeability));
	       
	    }
	  
	  }
	 
	}
	

	
//	neumannValues[contiNEqIdx]=(fs.massFraction(nPhaseIdx,Indices::nCompIdx)*mobilityGas*WellIndex*densityNw*permeability
//                     *(ProductionPressure-fs.pressure(nPhaseIdx)) + fs.massFraction(wPhaseIdx,Indices::nCompIdx)*mobilityWater*
//                             WellIndex*densityW*permeability*(ProductionPressure-fs.pressure(wPhaseIdx)))*(1.0/intersection.geometry().volume());
	
// 	if(time > 1 && time < 10800.0)
// 	{
// 	  neumannValues[contiNEqIdx] = mobilityGas*WellIndex*densityNw*permeability*(InjectionPressure-fs.pressure(nPhaseIdx))
// 	  *(1.0/intersection.geometry().volume()); //[kg/m²s]
// 	}
// 	if(time > 10800.0 && time < 21600.0)
// 	{
// 	    neumannValues[contiNEqIdx]=(fs.massFraction(nPhaseIdx,Indices::nCompIdx)*mobilityGas*WellIndex*densityNw*permeability
//                  *(ProductionPressure-fs.pressure(nPhaseIdx)) + fs.massFraction(wPhaseIdx,Indices::nCompIdx)*mobilityWater*
//                          WellIndex*densityW*permeability*(ProductionPressure-fs.pressure(wPhaseIdx)))*(1.0/intersection.geometry().volume());
// 	  
// 	    neumannValues[contiWEqIdx]=(fs.massFraction(nPhaseIdx,Indices::wCompIdx)*mobilityGas*WellIndex*densityNw*permeability
//                  *(ProductionPressure-fs.pressure(nPhaseIdx)) + fs.massFraction(wPhaseIdx,Indices::wCompIdx)*mobilityWater*
//                          WellIndex*densityW*permeability*(ProductionPressure-fs.pressure(wPhaseIdx)))*(1.0/intersection.geometry().volume());
// 	}

//  	  
 	Scalar injectionTemperature = 322; // wie bei Huntorf
//         neumannValues[energyEqIdx] = neumannValues[contiNEqIdx] * Air::gasEnthalpy(injectionTemperature, elemVolVars[scvIdx].pressure(nPhaseIdx));
        if(!isExtracting)
	  neumannValues[energyEqIdx] = neumannValues[contiNEqIdx] * Air::gasEnthalpy(injectionTemperature, InjectionPressure);
        
	else
	  neumannValues[energyEqIdx] = neumannValues[contiNEqIdx] * Air::gasEnthalpy(fs.temperature(nPhaseIdx), fs.pressure(nPhaseIdx));
// 	                                            +neumannValues[contiWEqIdx] * IapwsH2O::liquidEnthalpy(fs.temperature(wPhaseIdx), fs.pressure(nPhaseIdx));
	  
	

      }
    }

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param values The initial values for the primary variables
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        initial_(values, globalPos);
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     *
     * \param vertex The vertex
     * \param vIdxGlobal The global index of the vertex
     * \param globalPos The global position
     */
    int initialPhasePresence(const Vertex &vertex,
                             int &vIdxGlobal,
                             const GlobalPosition &globalPos) const
    {
        return bothPhases;
    }
    
    //write out data and plot with Gnuplot
   void postTimeStep()
    {
        ParentType::postTimeStep();
        Scalar inflow(0.0);
	Scalar pressure_in;
	Scalar mass_water_extracted;
	Scalar temperature_in;
	Scalar energy_inflow;
	Scalar pressure_well;
// 	Scalar inflow_sum(0.0);
//         Scalar outflow(0.0);
// 	Scalar outflow_sum(0.0);
//         Scalar simulationTime = this->timeManager().time();
//         Scalar temperature;
// 	Scalar temperature_top;
// 	Scalar temperature_middle;
// 	Scalar temperature_middleB;
//         Scalar pressure_t;
// 	Scalar pressure_b;
// 	Scalar energy(0.0);
// 	Scalar energy_b(0.0);
// 	Scalar gasEnthalpy;
// 	Scalar losses_r(0.0);
// 	Scalar losses_l(0.0);
// 	Scalar losses_b(0.0);
// 	Scalar losses_t(0.0);
//         energy = 0.0;

              // Loop over all elements
        for (auto element = this->gridView().template begin<0>(); element != this->gridView().template end<0>(); ++element)
        {
            const auto geometry = element->geometry();

            FVElementGeometry fvGeometry;
            fvGeometry.update(this->gridView(),*element);

            ElementVolumeVariables elemVolVars;
            elemVolVars.update(*this,
                               *element,
                                fvGeometry,
                                false /* oldSol? */);

            // loop over all intersections
            for (auto is = this->gridView().ibegin(*element); is != this->gridView().iend(*element); ++is)
            {
	      if(is->boundary())
                {
                    int fIdx = is->indexInInside();

                        const Scalar x = is->geometry().center()[0];
                        const Scalar y = is->geometry().center()[1];
			
			Scalar top = this->bBoxMax()[1];
			Scalar right = this->bBoxMax()[0];
			Scalar left = this->bBoxMin()[0];
			
// 			if(y > top - eps_ && x > right / 2.0 -10.0 && x < right / 2.0 + 10.0)
			if(y > top - 1.0 && x > right / 2.0 -0.5 && x < right / 2.0)
			{
			  PrimaryVariables neumannValues;
			  FluxVariables fluxVars;
			  fluxVars.update(*this, *element, fvGeometry, fIdx, elemVolVars, true);
// 			  FluxVariables fluxVars(*this, *element, fvGeometry, fIdx, elemVolVars, true);
			  int scvIdx = fluxVars.upstreamIdx(nPhaseIdx);
			  this->solDependentNeumann(neumannValues, *element, fvGeometry, *is, scvIdx, fIdx, elemVolVars);
			  
			  inflow += neumannValues[contiNEqIdx] * is->geometry().volume(); //[kg Air/s]
			  std::cout << "inflow" << inflow << std::endl;

			  FluidState fs = elemVolVars[scvIdx].fluidState();
			  pressure_in = fs.pressure(nPhaseIdx);
			  temperature_in = fs.temperature(nPhaseIdx);
			  energy_inflow = neumannValues[energyEqIdx] * is->geometry().volume();
			  Scalar timeStepSize = this->timeManager().timeStepSize(); 
			  massInjected_ += -1.0 * neumannValues[contiNEqIdx] * is->geometry().volume() * timeStepSize; //[kg Air]
			  energyInjected_ += -1.0 * neumannValues[energyEqIdx] * is->geometry().volume() * timeStepSize;
			  mass_water_extracted = neumannValues[contiWEqIdx];
			  pressure_well = injectionPressure_;
			  std::cout << "mass injected" << massInjected_ << std::endl;
			  std::cout << "pressure in aquifer" << pressure_in << std::endl;
			  std::cout << "energy-inflow" << energy_inflow << std::endl;
			  std::cout << "temperature in aquifer" << fs.temperature(nPhaseIdx) << std::endl;
			  std::cout << "volume" << is->geometry().volume() << std::endl;
			  
			  
			}
			
			
//                         if (y < eps_)
//                         {
// 			  pressure_b = elemVolVars[0].pressure();
// 			  // svcf on the bottom
//                           FluxVariables fluxVars(*this, *element, fvGeometry, fIdx, elemVolVars, true);
//                           outflow_sum += fluxVars.volumeFlux(0) * elemVolVars[fluxVars.upstreamIdx(0)].density();
// 			  outflow = fluxVars.volumeFlux(0) * elemVolVars[fluxVars.upstreamIdx(0)].density();
// 
// 
// 			if (x < this->bBoxMax()[0]/GET_RUNTIME_PARAM(TypeTag, Scalar, Grid.NumberOfCellsX)/2.0 + eps_)
// 			{
// 			  temperature = elemVolVars[0].temperature();
// 			  
// 			}
// 			
// 			energy_b += outflow * -1.0 * Air::gasEnthalpy(elemVolVars[fluxVars.upstreamIdx(0)].temperature(), elemVolVars[fluxVars.upstreamIdx(0)].pressure()) * this->timeManager().previousTimeStepSize();
// 
// 			if (x < this->bBoxMax()[0]/2.0 + eps_ && x < this->bBoxMax()[0]/2.0 - eps_)
// 			{
// 			   temperature_middleB = elemVolVars[0].temperature();
// 			}
//                         }
//                         if (y > this->bBoxMax()[1] - eps_)
//                         {
//                             // scvf on the top
//                             FluxVariables fluxVars(*this, *element, fvGeometry, fIdx, elemVolVars, true);
//                             inflow_sum += fluxVars.volumeFlux(0)*elemVolVars[fluxVars.upstreamIdx(0)].density();
// 			    inflow = fluxVars.volumeFlux(0)*elemVolVars[fluxVars.upstreamIdx(0)].density();
// 			    pressure_t = elemVolVars[0].pressure();
// 			   
// 			    if (elemVolVars[fluxVars.upstreamIdx(0)].temperature() > 333.3333)
// 			    {
// 			      energy += inflow * -1.0 * Air::gasEnthalpy(elemVolVars[fluxVars.upstreamIdx(0)].temperature(), elemVolVars[fluxVars.upstreamIdx(0)].pressure()) * this->timeManager().previousTimeStepSize();   
// 			    }
// 			    
// 			    if (x < this->bBoxMax()[0]/GET_RUNTIME_PARAM(TypeTag, Scalar, Grid.NumberOfCellsX)/2.0 + eps_)
// 			    {
// 			      temperature_top = elemVolVars[0].temperature();
// 			      
// 			    }
// 			    if (x < this->bBoxMax()[0]/2.0 + eps_ && x < this->bBoxMax()[0]/2.0 - eps_)
// 			    {
// 			      temperature_middle = elemVolVars[0].temperature();
// 			      
// 			    }
// 			  
// 			}
// 			
// 			if (x > this->bBoxMax()[0] - eps_)
// 			{
// 			  FluxVariables fluxVars(*this, *element, fvGeometry, fIdx, elemVolVars, true);
// 			  losses_r += -1/((1/50)+(1/70)+(0.1/0.035))*(273.15 - elemVolVars[0].temperature());
// 			  
// 			}
// 			if (x <  eps_)
// 			{
// 			  FluxVariables fluxVars(*this, *element, fvGeometry, fIdx, elemVolVars, true);
// 			  losses_l += -1/((1/50)+(1/70)+(0.1/0.035))*(273.15 - elemVolVars[0].temperature());
// 			  
// 			}
// 			if (y < eps_)
// 			{
// 			  FluxVariables fluxVars(*this, *element, fvGeometry, fIdx, elemVolVars, true);
// 			  losses_b += -1/((1/50)+(1/70)+(0.1/0.035))*(273.15 - elemVolVars[0].temperature());
// 			  
// 			}
// 			if (y > this->bBoxMax()[1] - eps_)
// 			{
// 			  FluxVariables fluxVars(*this, *element, fvGeometry, fIdx, elemVolVars, true);
// 			  losses_t += -1/((1/50)+(1/70)+(0.1/0.035))*(273.15 - elemVolVars[0].temperature());  
// 			}
		}
	    }
	}

//       // Gnuplot
//       // variables that don't go out of scope
      static double yMax = -1e100;
      static std::vector<double> x;
      static std::vector<double> y;
      static std::vector<double> y2;
      static std::vector<double> y3;
      static std::vector<double> y4;
      static std::vector<double> y5;
      static std::vector<double> y6;
      static std::vector<double> y7;
//       static std::vector<double> y7;
//       static std::vector<double> y3;
//       static std::vector<double> y4;
//       static std::vector<double> y5;
//       static std::vector<double> y6;
//       static std::vector<double> y7;
//       static std::vector<double> y8;
//       static std::vector<double> y9;
//       static std::vector<double> y10;
//       static std::vector<double> y11;
//       static std::vector<double> y12;
//       static std::vector<double> y13;
//       static std::vector<double> y14;
//       static double accumulatedMass = 0.0;
//       static double accumulatedEnergy = 0.0;
//       static double accumulatedEnergy_b = 0.0;
//       static double accumulatedLosses_s = 0.0;
//       static double accumulatedLosses_t = 0.0;
//       static double accumulatedLosses_b = 0.0;
// 
      Scalar inflow_plot = -1.0*inflow*1000;
      Scalar time_plot = this->timeManager().time()/3600.0;
      Scalar pressure_in_plot = pressure_in*1e-5;
      Scalar mass_injected_plot = massInjected_;
      Scalar temperature_in_plot = temperature_in - 273.15;
      Scalar energy_plot = -1.0 * energy_inflow / 1e3;
      Scalar energy_injected_plot = energyInjected_ / 1e6;
      Scalar pressure_well_plot = pressure_well *1e-5;
//       Scalar water_plot = mass_water_extracted * 1000.0;
      
//       double outflow_plot = -1.0*outflow_sum*1000;
//       pressure_t = pressure_t*1e-5;
//       pressure_b = pressure_b*1e-5;
//       temperature = temperature - 273.15;
//       temperature_middleB = temperature_middleB - 273.15;
//       temperature_top = temperature_top - 273.15;
//       temperature_middle = temperature_middle - 273.15;
//       accumulatedMass += 1e-6*inflow_plot*this->timeManager().previousTimeStepSize();
//       accumulatedEnergy += energy*1e-9;
//       accumulatedEnergy_b += energy_b * 1e-9;
//       accumulatedLosses_s += (losses_r + losses_l)*this->timeManager().previousTimeStepSize();
//       accumulatedLosses_t += losses_t*this->timeManager().previousTimeStepSize();
//       accumulatedLosses_b += losses_b*this->timeManager().previousTimeStepSize();
// 
// 
      x.push_back(time_plot);
//       y.push_back(inflow_plot);
      y.push_back(pressure_in_plot);
      y2.push_back(inflow_plot);
      y3.push_back(mass_injected_plot);
      y4.push_back(temperature_in_plot);
      y5.push_back(energy_plot);
      y6.push_back(energy_injected_plot);
      y7.push_back(pressure_well_plot);
//       y7.push_back(mass_water_extracted);
      
//       y3.push_back(accumulatedMass);
//       y4.push_back(temperature);
//       y5.push_back(outflow_plot);
//       y6.push_back(accumulatedEnergy);
//       y7.push_back(temperature_top);
//       y8.push_back(temperature_middle);
//       y9.push_back(accumulatedEnergy_b);
//       y10.push_back(temperature_middleB);
//       y11.push_back(pressure_b);
//       y12.push_back(accumulatedLosses_s);
//       y13.push_back(accumulatedLosses_t);
//       y14.push_back(accumulatedLosses_b);
// 

//       yMax = std::max({yMax, inflow_plot}); 
      yMax = std::max({yMax, pressure_in_plot}); 
//       yMax = std::max({yMax, inflow_plot, pressure_t, pressure_b, outflow_plot, accumulatedLosses_s, accumulatedLosses_b, accumulatedLosses_t});
//       y2Max = std::max({y2Max, accumulatedMass, temperature, accumulatedEnergy, temperature_top, temperature_middle, accumulatedEnergy_b, temperature_middleB});
// 
      gnuplot_.reset();
      gnuplot_.setXRange(0, x.back()+1);
      gnuplot_.setYRange(0.0, yMax+1);
    //  gnuplot_.setY2Range(0,y2Max+1);
      gnuplot_.setXlabel("Zeit [h]");
//       gnuplot_.setYlabel("Luftmassenstrom [g/s]");
      gnuplot_.setYlabel("verschiedene Einheiten");
//       gnuplot_.addDataSetToPlot(x, y, "Injizierter Luftmassenstrom am Brunnen", "axes x1y1 w l");
      gnuplot_.addDataSetToPlot(x, y, "Druck im Aquifer", "axes x1y1 w l");
      gnuplot_.addDataSetToPlot(x, y2, "Injizierter Luftmassenstrom", "axes x1y1 w l");
      gnuplot_.addDataSetToPlot(x, y3, "Injizierte Luftmasse", "axes x1y1 w l");
      gnuplot_.addDataSetToPlot(x, y4, "Temperatur im Aquifer", "axes x1y1 w l");
      gnuplot_.addDataSetToPlot(x, y5, "Energie-Fluss", "axes x1y1 w l");
      gnuplot_.addDataSetToPlot(x, y6, "Injizierte Energie", "axes x1y1 w l");
      gnuplot_.addDataSetToPlot(x, y7, "Druck im Brunnen", "axes x1y1 w l");
//       gnuplot_.addDataSetToPlot(x, y7, "Extrahierte Masse an Wasser", "axes x1y1 w l");
      

//       gnuplot2_.reset();
//       gnuplot2_.setXRange(0, x.back()+1);
//       gnuplot2_.setYRange(0, yMax+1);
//       gnuplot2_.setXlabel("Zeit [s]");
//       gnuplot2_.setYlabel("Luftmassenstrom [g/s]");
//       gnuplot2_.addDataSetToPlot(x, y, "inflow at well", "axes x1y1 w l");
      
//       gnuplot_.addDataSetToPlot(x, y2, "Druck im Aquifer", "axes x1y2 w l");
    
      
//       gnuplot_.addDataSetToPlot(x, y3, "cumulated mass", "axes x1y2 w l");
//       gnuplot_.addDataSetToPlot(x, y4, "temperature at bottom", "axes x1y2 w l");
//       gnuplot_.addDataSetToPlot(x, y5, "outflow at bottom", "axes x1y1 w l");
//       gnuplot_.addDataSetToPlot(x, y6, "cumulated energy", "axes x1y2 w l");
//       gnuplot_.addDataSetToPlot(x, y7, "temperature at top", "axes x1y2 w l");
//       gnuplot_.addDataSetToPlot(x, y8, "temperature middle", "axes x1y2 w l");
//       gnuplot_.addDataSetToPlot(x, y9, "cumulated energy at bottom", "axes x1y2 w l");
//       gnuplot_.addDataSetToPlot(x, y10, "temperature bottom-middle", "axes x1y2 w l");
//       gnuplot_.addDataSetToPlot(x, y11, "pressure at bottom", "axes x1y1 w l");
//       gnuplot_.addDataSetToPlot(x, y12, "losses Seiten", "axes x1y1 w l");
//       gnuplot_.addDataSetToPlot(x, y13, "losses Top", "axes x1y1 w l");
//       gnuplot_.addDataSetToPlot(x, y14, "losses Bottom", "axes x1y1 w l");
//       // second axis
//        gnuplot_.setOption("set y2label \"Druck im [bar]\"");
//        std::ostringstream stream;
//        stream << "set y2range [" << 0.0 << ":" << y2Max+1 << "]";
//        gnuplot_.setOption(stream.str());
//        gnuplot_.setOption("set ytics nomirror");
//        gnuplot_.setOption("set y2tics");
// 
      gnuplot_.plot("inflow_plot");
//      gnuplot2_.plot("pressure_in_plot");
    }

private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
      Scalar densityW = 1000.0;
      Scalar densityNw = 60.0;
//       values[pressureIdx] = 1e5 + (maxDepth_ - globalPos[1])*densityW*9.81;
      values[pressureIdx] = 59.008e5 + (25.0 - globalPos[1])*densityW*9.81; // 60bar am Brunnen
      Scalar plumeDistance = 15.0;
      if(globalPos[1] < plumeDistance)
	values[switchIdx] = 0.001;
      else
      {
	Scalar entryP = 1e4;//has to be as in SpatialParams
	Scalar lambda = 2.0;//has to be as in SpatialParams
	Scalar resSatW = 0.2;//has to be as in SpatialParams
	Scalar resSatN = 0.0;//has to be as in SpatialParams
	values[switchIdx] = 1.0-(std::pow(((globalPos[1]-plumeDistance)*(densityW-densityNw)*this->gravity().two_norm()+entryP),(-lambda))
                            * std::pow(entryP,lambda) * (1.0-resSatW-resSatN)+resSatW);//only works for Brooks-Corey
      }
#if !ISOTHERMAL
//         values[temperatureIdx] = 283.0 + (maxDepth_ - globalPos[1])*0.03;
        values[temperatureIdx] = 313.1329 + (25.0 - globalPos[1])*0.03; //40°C am Brunnen
#endif
     }

    Scalar maxDepth_;
    Scalar eps_;
    std::string name_;
    Scalar massInjected_;
    Scalar mutable injectionPressure_;
    Scalar energyInjected_;
    Scalar massInjectedTimestep_;
    Dumux::GnuplotInterface<double> gnuplot_;
    Dumux::GnuplotInterface<double> gnuplot2_;
    
};
} //end namespace

#endif
